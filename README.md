django-authhacks
================

Provide USERNAME_MAXLENGTH settings variable

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/django-authhacks>
* GitHub: <https://github.com/sikaondrej/django-authhacks>

## Documentation

### Instalation

Instalation is very simple over pip.

    # pip install django-authhacks

add to `INSTALLED_APPS` in `settings.py`

    INSTALLED_APPS += ("authhacks", )
    USERNAME_MAXLENGTH = 127
